package com.example.myapplication.model;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;


public class Utilities {
    public static final String DINNER_TIME = "18:00";
    public static final String LUNCH_TIME = "12:00";
    public static final String BREAKFAST_TIME = "06:00";

    public static Location currentLocation;
    public static String curDate;
    public static List<Location> locations;
    public static HashMap<Meal, ArrayList<Location>> meals = new HashMap<>();
    private ObjectMapper mapper;

    public Utilities() {
    }

    private static int minutesSinceMidnight(String time) {
        if (time.length() == 6) {
            //need to add leading 0 if time is in format "h:mmAM/PM"
            time = "0" + time;
        }
        String AMPM = time.substring(5).toLowerCase();
        time = time.substring(0, 5);
        String[] hoursMinutes = time.split(":");
        int minutes = Integer.parseInt(hoursMinutes[1]);
        int hours = Integer.parseInt(hoursMinutes[0]);
        hours = (hours == 12) ? 0 : hours;
        hours += (AMPM.equals("pm")) ? 12 : 0;
        System.out.println(hours*60+minutes);
        return hours * 60 + minutes;
    }

    public static boolean isOpen(Location location, String time) {
        if (location.getLocationInfo() == null) return true;
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE", Locale.US); // the day of the week spelled out completely
        String weekday = formatter.format(date);
        String hours = location.getLocationInfo().getHours().get(weekday).get(0);
        String[] openClosed = hours.split("-");
        String open = openClosed[0];
        String closed = openClosed[1];
        int openMinutes = minutesSinceMidnight(open);
        int closedMinutes = minutesSinceMidnight(closed);
        int currentMinutes = minutesSinceMidnight(time);
        if(closedMinutes < openMinutes){
            closedMinutes += 60*24;
        }
        return openMinutes <= currentMinutes && currentMinutes <= closedMinutes;
    }

    public static Location getLocation(String name) {
        for (Location loc : locations) {
            if (loc.getName().toLowerCase().equals(name.toLowerCase())) {
                return loc;
            }
        }
        return null;
    }

    public static void setCurrentLocation(Location currentLocation) {
        Utilities.currentLocation = currentLocation;
    }

    public static Map<String, MealMenu> getMealsByDate() {
        if (currentLocation.getMenuCollection().getDayMenus().get(curDate) != null) {
            return Objects.requireNonNull(currentLocation.getMenuCollection().getDayMenus().get(curDate)).getMeals();
        }
        return null;
    }

    public static MealMenu getMealsByDateAndTime(String date, String timeStr) {
        ArrayList<String> times = new ArrayList<>();
        times.add(BREAKFAST_TIME);
        times.add(LUNCH_TIME);
        times.add(DINNER_TIME);
        HashMap<String, String> mealTimes = new HashMap<>();
        mealTimes.put(BREAKFAST_TIME, "Breakfast");
        mealTimes.put(LUNCH_TIME, "Lunch");
        mealTimes.put(DINNER_TIME, "Dinner");
        String timeOfDay;
        if (times.stream().anyMatch(time -> time.equals(timeStr))) {
            timeOfDay = mealTimes.get(timeStr);
        } else {
            times.add(timeStr);
            times.sort(String::compareTo);
            timeOfDay = mealTimes.get(times.get(times.lastIndexOf(timeStr) - 1));
        }
        if (date.charAt(0) == '0') {
            date = date.substring(1);
        }
        if (currentLocation.getMenuCollection().getDayMenus().get(date) != null) {
            return Objects.requireNonNull(currentLocation.getMenuCollection().getDayMenus().get(date)).getMeals().get(timeOfDay);
        } else return null;
    }

    public static Map<String, MealMenu> getMealsCurrentDateAndTime() {
        // TODO: delete the unused variable
        ArrayList<String> times = new ArrayList<>();
        times.add(BREAKFAST_TIME);
        times.add(LUNCH_TIME);
        times.add(DINNER_TIME);
        HashMap<String, String> mealTimes = new HashMap<>();
        mealTimes.put(BREAKFAST_TIME, "Breakfast");
        mealTimes.put(LUNCH_TIME, "Lunch");
        mealTimes.put(DINNER_TIME, "Dinner");
        boolean equal = false;
        String timeStr = getCurrentTime();
        String date = getCurrentDate();
        String timeOfDay;
        if (times.stream().anyMatch(time -> time.equals(timeStr))) {
            timeOfDay = mealTimes.get(timeStr);
        } else {
            times.add(timeStr);
            times.sort(String::compareTo);
//            timeOfDay = mealTimes.get(times.get(times.lastIndexOf(timeStr) - 1)); // don't need this anymore
        }
        date = (date.charAt(3) == '0') ? date.substring(0, 3) + date.substring(4) : date;
        date = (date.charAt(0) == '0') ? date.substring(1) : date;
        if (currentLocation.getMenuCollection().getDayMenus().get("3/23/2020") != null) {
            return Objects.requireNonNull(currentLocation.getMenuCollection().getDayMenus().get("3/23/2020")).getMeals();
        } else return null;
    }

    /***
     * Gets the current date in mm/dd/yyyy format and returns it as a String
     * @return
     */
    public static String getCurrentDate() {
        Date currentDate = Calendar.getInstance().getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
        return dateFormat.format(currentDate);
    }

    public static String isValidPickupTime(String time){
        currentLocation = getLocation("TJ Bistro");
        String currentTime = getCurrentTimeStandard();
        int curTimeMinutes = minutesSinceMidnight(currentTime);
        int enteredTimeMinutes = minutesSinceMidnight(time);
        //time needs to be while the place is open and after current time
        if(isOpen(currentLocation,getCurrentTimeStandard())) {
            if (isOpen(currentLocation, time)) {
                if (enteredTimeMinutes >= curTimeMinutes) {
                    return "valid";
                }
                return "beforeCurrentTime";
            }
            return "timeAfterClose";
        }
        return "closed";
    }

    /***
     * Returns a string of the current time in 24 hr format
     * @return
     */
    public static String getCurrentTime() {
        Date currentDate = Calendar.getInstance().getTime();
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.US);
        return timeFormat.format(currentDate);
    }

    public static String getCurrentTimeStandard() {
        Date currentDate = Calendar.getInstance().getTime();
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mmaa", Locale.US);
        return timeFormat.format(currentDate);
    }

    public static void setMeals() {
        for (Location location : Utilities.locations) {
            Utilities.currentLocation = location;
            Map<String, MealMenu> menus = Utilities.getMealsByDate();
            assert menus != null;
            menus.forEach((time, mealSet) -> {
                mealSet.getCategories().forEach((heading, meals) -> {
                    meals.forEach(meal -> {
                        if (!Utilities.meals.containsKey(meal)) {
                            Utilities.meals.put(meal, new ArrayList<>(Collections.singletonList(location)));
                        } else {
                            if (!Utilities.meals.get(meal).contains(location)) {
                                Utilities.meals.get(meal).add(location);
                            }
                        }
                    });
                });
            });
        }
    }

    public static void emailFeedback(View view) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        String[] recipient = {"feedback@westfield.ma.edu"};
        intent.setType("plain/text");
        intent.putExtra(Intent.EXTRA_EMAIL, recipient);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Dining Experience");
        intent.putExtra(Intent.EXTRA_TEXT, "");
        Context context = view.getContext();
        context.startActivity(intent);
    }

  /***
   * Send to go order via email to specified location
   * @param view
   * @param location where order is being sent and will be picked up
   * @param cart a hashmap of meal items along with the quantity of each item.
   * @param time the time the user wishes to pick up their order.
   */
  public static void emailOrder(View view, String location, ShoppingCart cart, String time) {
    Intent intent = new Intent(Intent.ACTION_SEND);
    String[] recipient = {location + "@westfield.ma.edu"};
    intent.setType("plain/text");
    intent.putExtra(Intent.EXTRA_EMAIL, recipient);
    intent.putExtra(Intent.EXTRA_SUBJECT, "To-Go Order");
    StringBuilder orderFormatted = new StringBuilder();
      for (CartItem item : cart.getItems()) {
          orderFormatted.append("Item : ").append(item.getMeal().getMealName()).append("\n");
          orderFormatted.append("Quantity : ").append(item.getQuantity()).append("\n");
      }
      intent.putExtra(Intent.EXTRA_TEXT, orderFormatted.toString());
    Context context = view.getContext();
    context.startActivity(intent);
  }

    @Override
    public String toString() {
        String res = "";
        for (Location location : locations) {
            res += location.toString() + "\n";
        }
        return res;
    }
}
