package com.example.myapplication.model;

import java.util.List;
import java.util.Objects;

public class Meal {
    private String mealName;
    private List<String> allergies;

    // for mapper
    public Meal() {

    }

    public Meal(String mealName, List<String> allergies) {
        this.mealName = mealName;
        this.allergies = allergies;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public List<String> getAllergies() {
        return allergies;
    }

    public void setAllergies(List<String> allergies) {
        this.allergies = allergies;
    }

    public void addAllergy(String allergy) {
        this.allergies.add(allergy);
    }

    public void removeAllergy(String allergy) {
        allergies.remove(allergy);
    }

    public String getAllergiesInString() {
        StringBuilder sb = new StringBuilder();
        sb.append('\n');
        for (String a : allergies) {
            sb.append(a);
            sb.append(" ");
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Meal meal = (Meal) o;
        return Objects.equals(mealName, meal.mealName) &&
                Objects.equals(allergies, meal.allergies);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mealName, allergies);
    }

    public String toString() {
        return "Meal{" +
                "mealName='" + mealName + '\'' +
                ", allergies=" + allergies +
                '}';
    }
}
