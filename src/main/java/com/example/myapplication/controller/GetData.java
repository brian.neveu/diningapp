package com.example.myapplication.controller;


import android.os.AsyncTask;

import com.example.myapplication.model.Location;
import com.example.myapplication.model.Utilities;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URL;
import java.util.List;

public class GetData extends AsyncTask<Void, Void, Void> {
    @Override
    protected Void doInBackground(Void... voids) {
        List<Location> diningMenus = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            URL url = new URL("https://wsu2020tree.s3.amazonaws.com/current-menu.json");
            diningMenus = mapper.readValue(url, new TypeReference<List<Location>>() { });
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error reading JSON");
        }
        Utilities.locations = diningMenus;
        Utilities.curDate = "3/23/2020";
        assert Utilities.locations != null;
        return null;
    }

    @Override
    protected void onPostExecute(Void res) {
        ;
    }
}
