package com.example.myapplication.controller;


import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.myapplication.R;
import com.example.myapplication.model.Location;
import com.example.myapplication.model.Utilities;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class LocationPage extends Fragment {


    public LocationPage() {
        // Required empty public constructor
    }

    private String writeBlankChars(int length) {
        String res = "";
        for (int i = 0; i < length; ++i) {
            res = res + " ";
        }
        return res;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        String locationFragTitle = this.getArguments().getString("location");
        View view = inflater.inflate(R.layout.fragment_location_page, container, false);
        HashMap<String, ArrayList<Integer>> images = new HashMap<>();
        images.put("Ely Harvest", new ArrayList<>(Arrays.asList(R.drawable.ely1, R.drawable.ely2, R.drawable.ely3, R.drawable.ely4)));
        images.put("Garden Café", new ArrayList<>());
        images.put("Wilson Café", new ArrayList<>(Arrays.asList(R.drawable.wilson1, R.drawable.wilson2, R.drawable.wilson3, R.drawable.wilson4)));
        images.put("Marketplace", new ArrayList<>());
        images.put("TJ Bistro", new ArrayList<>(Collections.singletonList(R.drawable.bistro1)));
        images.put("Tim & Jeanne's Dining Commons", new ArrayList<>(Arrays.asList(R.drawable.dc1, R.drawable.dc2, R.drawable.dc3, R.drawable.dc4)));

        HashMap<String, Integer> locationImages = new HashMap<>();
        locationImages.put("Wilson Café", R.drawable.wilsoncafe);
        locationImages.put("Garden Café", R.drawable.gardencafe);
        locationImages.put("Marketplace", R.drawable.marketplace);
        locationImages.put("Ely Harvest", R.drawable.elyharvest);
        locationImages.put("Tim & Jeanne's Dining Commons", R.drawable.timjeannedining);
        locationImages.put("TJ Bistro", R.drawable.tjbistro);

        TextView locationTitle = view.findViewById(R.id.locTitle);
        TextView locationDesc = view.findViewById(R.id.locDesc);
        TextView mondayHours = view.findViewById(R.id.monday);
        TextView tuesdayHours = view.findViewById(R.id.tuesday);
        TextView wednesdayHours = view.findViewById(R.id.wednesday);
        TextView thursdayHours = view.findViewById(R.id.thursday);
        TextView fridayHours = view.findViewById(R.id.friday);
        TextView saturdayHours = view.findViewById(R.id.saturday);
        TextView sundayHours = view.findViewById(R.id.sunday);

        Location location = Utilities.getLocation(locationFragTitle);
        Map<String, List<String>> map = location.getLocationInfo().getHours();
        locationTitle.setText(location.getName());
        locationDesc.setText(location.getDescription());
        mondayHours.setText("Monday: " + map.get("Monday").get(0));
        tuesdayHours.setText("Tuesday: " + map.get("Tuesday").get(0));
        wednesdayHours.setText("Wednesday: " + map.get("Wednesday").get(0));
        thursdayHours.setText("Thursday: " + map.get("Thursday").get(0));
        fridayHours.setText("Friday: " + map.get("Friday").get(0));
        // Need to check this because some location is not open on the weekend
        // if so, get(int index) would be out of bound
        if (map.get("Saturday").size() > 0) {
            saturdayHours.setText("Saturday: " + map.get("Saturday").get(0));
        } else {
            saturdayHours.setText("Saturday: closed");
        }
        if (map.get("Sunday").size() > 0) {
            sundayHours.setText("Sunday: " + map.get("Sunday").get(0));
        } else {
            sundayHours.setText("Sunday: closed");
        }


        //TODO: insert correct images below
        ImageView mapLocation = view.findViewById(R.id.mapImage);
        Integer drawableID = locationImages.get(locationFragTitle);
        Picasso.get().load(drawableID).fit().into(mapLocation);
        LinearLayout imageLayout = view.findViewById(R.id.locImgLayout);
        ArrayList<Integer> imageSet = images.get(locationFragTitle);
        if (imageSet.isEmpty()) {
            LinearLayout defaultLoc = (LinearLayout) inflater.inflate(R.layout.no_image_default, container, false);
            imageLayout.addView(defaultLoc);
        } else {
            imageSet.forEach(id -> {
                float imageWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300, getResources().getDisplayMetrics());
                ImageView image = new ImageView(view.getContext());
                image.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) imageWidth));
                Picasso.get().load(id).resize(550, 400).into(image);
                image.setAdjustViewBounds(true);
                image.setScaleType(ImageView.ScaleType.FIT_XY);
                int rightMargin = (id.equals(imageSet.get(imageSet.size() - 1))) ? (int) (imageWidth / 30) : 0;
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(image.getLayoutParams());
                params.setMargins((int) (imageWidth / 30), 0, rightMargin, 0);
                image.setLayoutParams(params);
                imageLayout.addView(image);
            });
        }


        return view;
    }

}
