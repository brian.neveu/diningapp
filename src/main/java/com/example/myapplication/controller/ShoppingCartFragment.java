package com.example.myapplication.controller;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.example.myapplication.R;
import com.example.myapplication.model.ShoppingCart;
import com.example.myapplication.model.Utilities;


/**
 * A simple {@link Fragment} subclass.
 */
public class ShoppingCartFragment extends Fragment {

    private View myView;

    public ShoppingCartFragment() {
        // Required empty public constructor
    }

    private void emptyCart(LinearLayout cartLayout, TextView empty){
        for(int i = 1; i < cartLayout.getChildCount(); ++i){
            cartLayout.getChildAt(i).setVisibility(View.GONE);
        }
        empty.setVisibility(View.VISIBLE);
    }

    private void showCart(LinearLayout cartLayout, TextView empty){
        for(int i = 1; i < cartLayout.getChildCount(); ++i){
            cartLayout.getChildAt(i).setVisibility(View.VISIBLE);
        }
        empty.setVisibility(View.GONE);
    }

    public void updateUI(){
        LinearLayout cartLayout = myView.findViewById(R.id.cartLayout);
        LinearLayout cartOuterLayout = myView.findViewById(R.id.cartOuterLayout);
        TextView empty = myView.findViewById(R.id.emptyCartText);
        ShoppingCart cart = ShoppingCart.getInstance();
        if(cart.getItems().isEmpty()){
            emptyCart(cartOuterLayout,empty);
        }
        else {
            showCart(cartOuterLayout,empty);
            for (int i = 0; i < cartLayout.getChildCount() - 2; ++i) {
                cartLayout.removeViewAt(i);
            }
            cart.getItems().forEach(cartItem -> {
                CardView itemCard = (CardView) LayoutInflater.from(myView.getContext()).inflate(R.layout.item_card, cartLayout, false);
                TextView upArrow = itemCard.findViewById(R.id.upArrow);
                TextView downArrow = itemCard.findViewById(R.id.downArrow);
                TextView itemNum = itemCard.findViewById(R.id.itemNum);
                TextView mealName = itemCard.findViewById(R.id.itemMealName);
                TextView locName = itemCard.findViewById(R.id.itemLocName);
                locName.setText(cartItem.getLocation().getName());
                mealName.setText(cartItem.getMeal().getMealName());
                itemNum.setText(Integer.toString(cartItem.getQuantity()));
                TextView remove = itemCard.findViewById(R.id.cartItemRemove);
                upArrow.setOnClickListener(listener -> {
                    cartItem.addOne();
                    int newQty = cartItem.getQuantity();
                    itemNum.setText(Integer.toString(newQty));
                });
                downArrow.setOnClickListener(listener -> {
                    cartItem.subtractOne();
                    int newQty = cartItem.getQuantity();
                    itemNum.setText(Integer.toString(newQty));
                });
                remove.setOnClickListener(listener -> {
                    cartLayout.removeView(itemCard);
                    cart.deleteItem(cartItem);
                    TextView customToast = (TextView) LayoutInflater.from(myView.getContext()).inflate(R.layout.custom_toast, cartLayout, false);
                    Toast toast = Toast.makeText(myView.getContext(), "Item removed from cart.", Toast.LENGTH_SHORT);
                    customToast.setText("Item removed from cart.");
                    toast.setView(customToast);
                    toast.show();
                    //if the number of items is now 0, cart should act as if it's empty
                    if(cart.getItems().isEmpty()){
                        emptyCart(cartOuterLayout,empty);
                    }
                });
                cartLayout.addView(itemCard, 0);
            });
        }
    }

    private void displayErrorMsgSubmit(String errorType){
        LinearLayout cartLayout = myView.findViewById(R.id.cartLayout);
        TextView customToast = (TextView) LayoutInflater.from(myView.getContext()).inflate(R.layout.custom_toast, cartLayout, false);
        Toast toast = Toast.makeText(myView.getContext(), "Item removed from cart.", Toast.LENGTH_SHORT);
        String text = "";
        switch(errorType){
            case "beforeCurrentTime":
                text = "Time for pickup cannot be before current time.";
                break;
            case "closed":
                text = "Place is currently closed right now.";
                break;
            case "timeAfterClose":
                text = "Can't select a time while the place is closed.";
                break;
        }
        customToast.setText(text);
        toast.setView(customToast);
        toast.show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_shopping_cart, container, false);
        updateUI();
        TextView submit = myView.findViewById(R.id.submitOrder);
        TimePicker timePicker = myView.findViewById(R.id.timePicker);
        submit.setOnClickListener(listener->{
            int hour = timePicker.getHour();
            String AMPM = (hour >= 12) ? "PM" : "AM";
            String hourStr = Integer.toString(hour);
            String minuteStr = Integer.toString(timePicker.getMinute());
            if(hourStr.equals("0")){
                hourStr = "00";
            }
            if(minuteStr.equals("0")){
                minuteStr = "00";
            }
            String time = hourStr +":"+minuteStr+"AM";
            String valid = Utilities.isValidPickupTime(time);
            if(!valid.equals("valid")){
                displayErrorMsgSubmit(valid);
            }
            else {
                Utilities.emailOrder(myView,"TJ Bistro",ShoppingCart.getInstance(),time);
            }
        });
        return myView;
    }

}
