package com.example.myapplication.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.example.myapplication.R;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ImageArrayAdapter extends ArrayAdapter<String> {

    Context context;
    List<String> imageURLs;

    public ImageArrayAdapter(@NonNull Context context, int resource, @NonNull List<String> imageURLs) {
        super(context, resource, imageURLs);

        this.context = context;
        this.imageURLs = imageURLs;
    }

    //called when rendering the list
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //get the property we are displaying
        String url = imageURLs.get(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.specialevent_image_layout, null);

        ImageView imageView = view.findViewById(R.id.specialEventPoster);
        if(position==0){
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)imageView.getLayoutParams();
            float ten = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, parent.getResources().getDisplayMetrics());
            params.topMargin = (int)ten;
            imageView.setLayoutParams(params);
        }
        Picasso.get().load(url).fit().into(imageView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // show zoom image dialog when image is clicked
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                View mView = ((Activity) context).getLayoutInflater().inflate(R.layout.dialog_customzoom_layout, null);

                PhotoView photoView = mView.findViewById(R.id.imageView);
                photoView.setImageDrawable(scaleImage(imageView.getDrawable(), 1.5f));

                builder.setView(mView);
                AlertDialog dialog = builder.create();

                mView.findViewById(R.id.btn_dismiss).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        return view;
    }

    public Drawable scaleImage(Drawable image, float scaleFactor) {

        if ((image == null) || !(image instanceof BitmapDrawable)) {
            return image;
        }

        Bitmap b = ((BitmapDrawable) image).getBitmap();

        int sizeX = Math.round(image.getIntrinsicWidth() * scaleFactor);
        int sizeY = Math.round(image.getIntrinsicHeight() * scaleFactor);

        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, sizeX, sizeY, false);

        image = new BitmapDrawable(context.getResources(), bitmapResized);

        return image;
    }
}
